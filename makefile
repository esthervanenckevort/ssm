# makefile
# Created by IBM WorkFrame/2 MakeMake at 6:05:55 on 21 July 1999
#
# The actions included in this make file are:
#  Compile::C++ Compiler
#  Link::Linker
#  Bind::Resource Bind
#  Compile::Resource Compiler

.SUFFIXES: .c .obj .rc .res 

.all: \
    .\ssm.exe

.c.obj:
    @echo " Compile::C++ Compiler "
    icc.exe /Tdp /Q /Ti /C %s

{e:\data\c\ssm}.c.obj:
    @echo " Compile::C++ Compiler "
    icc.exe /Tdp /Q /Ti /C %s

.rc.res:
    @echo " Compile::Resource Compiler "
    rc.exe -r %s %|dpfF.RES

{e:\data\c\ssm}.rc.res:
    @echo " Compile::Resource Compiler "
    rc.exe -r %s %|dpfF.RES

.\ssm.exe: \
    .\support.obj \
    .\test.obj \
    .\find.obj \
    .\main.obj \
    .\pmtools.obj \
    .\prodinfo.obj \
    .\question.obj \
    .\settings.obj \
    .\ssm.obj \
    .\ssm.res \
    {$(LIB)}os2386.lib \
    {$(LIB)}mmpm2.lib \
    {$(LIB)}cppom30.lib \
    {$(LIB)}ssm.def
    @echo " Link::Linker "
    @echo " Bind::Resource Bind "
    icc.exe @<<
     /B" /de /exepack:2 /pmtype:pm /packd /nologo"
     /Fessm.exe 
     os2386.lib 
     mmpm2.lib 
     cppom30.lib 
     ssm.def
     .\support.obj
     .\test.obj
     .\find.obj
     .\main.obj
     .\pmtools.obj
     .\prodinfo.obj
     .\question.obj
     .\settings.obj
     .\ssm.obj
<<
    rc.exe .\ssm.res ssm.exe

.\support.obj: \
    .\support.c \
    .\ssm.h \
    .\support.h \
    .\main.h \
    .\acceltable.h \
    .\stringtable.h \
    .\test.h \
    .\menu.h \
    .\question.h \
    .\find.h \
    .\icon.h \
    .\prodinfo.h \
    .\settings.h \
    .\pmtools.h

.\ssm.obj: \
    .\ssm.c \
    .\ssm.h \
    .\support.h \
    .\main.h \
    .\acceltable.h \
    .\stringtable.h \
    .\test.h \
    .\menu.h \
    .\question.h \
    .\find.h \
    .\icon.h \
    .\prodinfo.h \
    .\settings.h \
    .\pmtools.h

.\settings.obj: \
    .\settings.c \
    .\ssm.h \
    .\support.h \
    .\main.h \
    .\acceltable.h \
    .\stringtable.h \
    .\settings.h

.\question.obj: \
    .\question.c \
    .\ssm.h \
    .\support.h \
    .\main.h \
    .\acceltable.h \
    .\stringtable.h \
    .\question.h

.\prodinfo.obj: \
    .\prodinfo.c \
    .\ssm.h \
    .\support.h \
    .\main.h \
    .\acceltable.h \
    .\stringtable.h \
    .\test.h \
    .\menu.h \
    .\question.h \
    .\find.h \
    .\icon.h \
    .\prodinfo.h \
    .\settings.h \
    .\pmtools.h

.\pmtools.obj: \
    .\pmtools.c

.\main.obj: \
    .\main.c \
    .\ssm.h \
    .\support.h \
    .\main.h \
    .\acceltable.h \
    .\stringtable.h \
    .\test.h \
    .\menu.h \
    .\question.h \
    .\find.h \
    .\icon.h \
    .\prodinfo.h \
    .\settings.h \
    .\pmtools.h

.\find.obj: \
    .\find.c \
    .\ssm.h \
    .\support.h \
    .\main.h \
    .\acceltable.h \
    .\stringtable.h \
    .\test.h \
    .\menu.h \
    .\question.h \
    .\find.h \
    .\icon.h \
    .\prodinfo.h \
    .\settings.h \
    .\pmtools.h

.\test.obj: \
    .\test.c \
    .\ssm.h \
    .\support.h \
    .\main.h \
    .\acceltable.h \
    .\stringtable.h \
    .\test.h

.\ssm.res: \
    .\ssm.rc \
    .\settings.dlg \
    .\settings.h \
    .\prodinfo.dlg \
    .\prodinfo.h \
    .\test.dlg \
    .\test.h \
    .\find.dlg \
    .\find.h \
    .\question.dlg \
    .\question.h \
    .\main.dlg \
    .\main.h \
    .\ssm.ico \
    .\acceltable.h \
    .\stringtable.h \
    .\icon.h \
    .\menu.h
